//
//  RecipeVCv2.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 21.08.2021.
//

import UIKit


class RecipeVCv2: UIViewController {
    
    
    @IBAction func addButtonAction(_ sender: Any) {
        switch self.recipe!.isSaved {
        case true:
            unsaveRecipe(recipe: recipe!)
//            print("recipe unsaved")
            self.recipe!.isSaved = false
            saveBtn.title = "Save"
        default:
            saveRecipe(recipe: recipe!)
//            print("recipe saved")
            self.recipe!.isSaved = true
            saveBtn.title = "Unsave"
        }
//        saveRecipe(recipe: recipe!)
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var recImage: UIImageView!
    @IBOutlet weak var navigationBar: UINavigationItem!
    @IBOutlet weak var instructionsLbl: UILabel!
    @IBOutlet weak var ingredientsLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var backBtn: UIBarButtonItem!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    
    
    
    var fromViewNamed: String?
    var recipe: Recipe?
//    var isSaved: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.leftBarButtonItem = backBtn
        navigationBar.rightBarButtonItem = saveBtn
        nameLbl.text = recipe!.name
        recImage.image = recipe!.image
        instructionsLbl.text = combineStepsText(instructions: recipe!.instructions)
        ingredientsLbl.text = combineIngredientsText(ingredients: recipe?.ingredients)
        
        setSaveButton()
        
        
        
//        switch fromViewNamed {
//        case "Find":
//            addButton.alpha = 1
//        case "Saved":
//            addButton.alpha = 0
//        default:
//            addButton.alpha = addButton.alpha
//        }
    }
    
    func setSaveButton () {
        switch self.recipe!.isSaved {
        case true:
            saveBtn.title = "Unsave"
        case false:
            saveBtn.title = "Save"
        }
    }
    
    
    
    
}

