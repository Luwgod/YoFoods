//
//  tastyApi.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 01.09.2021.
//

import Foundation

let headers = [
    "x-rapidapi-host": "tasty.p.rapidapi.com",
    "x-rapidapi-key": "1c0c203cb0msh6104b035bf77881p106367jsn76acab46647a"
]

let request = NSMutableURLRequest(url: NSURL(string: "https://tasty.p.rapidapi.com/recipes/list?from=0&size=20&q=ice%20cream")! as URL,
                                        cachePolicy: .useProtocolCachePolicy,
                                    timeoutInterval: 10.0)
request.httpMethod = "GET"
request.allHTTPHeaderFields = headers

let session = URLSession.shared
let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
    if (error != nil) {
        print(error)
    } else {
        let httpResponse = response as? HTTPURLResponse
        print(httpResponse)
    }
})

dataTask.resume()
