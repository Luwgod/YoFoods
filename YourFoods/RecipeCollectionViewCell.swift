//
//  RecipeCollectionViewCell.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 24.11.2021.
//

import UIKit

class RecipeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var recipeNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var ingredientsNumLabel: UILabel!
    
    func configureRecipeCell(recipe: Recipe) {
        
        if let image = recipe.image {
            recipeImageView.image = image
        } else if let urlString = recipe.thumbnailUrl {
            NetworkRequest.shared.requestData(urlString: urlString) { [weak self] result in
                switch result {
                case .success(let data):
                    let image = UIImage(data: data)
                    self?.recipeImageView.image = image
                case .failure(let error):
                    self?.recipeImageView.image = nil
                    print(error.localizedDescription)
                }
            }
        } else {
            recipeImageView.image = nil
        }
        
        recipeNameLabel.text = recipe.name
        timeLabel.text = "10 minutes"
        ingredientsNumLabel.text = "3"
        
        self.layer.cornerRadius = 10
        
    }
    
}
