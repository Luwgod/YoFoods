//
//  Model.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 18.08.2021.
//

import Foundation
import SwiftUI
import CoreData
import Firebase


var models = [RecipeItem]()

struct Recipe {
    let id: Int
    let name: String
    let ingredients: [String]
    let instructions: [String]
    var image: UIImage?
    let thumbnailUrl: String?
}


// Сохранение рецепта в CoreData

func saveRecipe(recipe: Recipe) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let newItem = RecipeItem(context: context)
    
    newItem.name = recipe.name
    newItem.image = recipe.image?.pngData()
    newItem.instructions = recipe.instructions
    newItem.ingredients = recipe.ingredients

    models.append(newItem)
    
    
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    do {
        try context.save()
    } catch {
         
    }
}


// Удаление рецепта из CoreData

func unsaveRecipe (recipe: Recipe) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "RecipeItem")
    fetch.predicate = NSPredicate(format: "name == %@", recipe.name)
    do {
        let item = try context.fetch(fetch)[0] as! RecipeItem
        context.delete(item)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
    } catch {
    }
    
}


func saveRecipeToFirebase(recipeId: Int) {
    let user = Auth.auth().currentUser
    guard let uid = user?.uid else { return }
    
    let ref = Database.database(url: "https://yofoods-99fef-default-rtdb.europe-west1.firebasedatabase.app").reference()
    let usersSavedRecipesRef = ref.child("users").child(uid).child("saved")
    let values = ["recipe: \(recipeId)": recipeId]
    usersSavedRecipesRef.updateChildValues(values) { err, ref in
        if err != nil {
            print(err)
            return
        }
    }
}


// Получить список ингредиентов

func getIngredientsArray (recipe: RecipeModel?) -> [String] {
    var ingredientsArray: [String] = []
    if let sections = recipe?.sections {
        for component in sections[0].components {
            ingredientsArray.append(component.raw_text)
        }
    }
    
    return ingredientsArray
}


// Получить список шагов(инструкцию)

func getInstructionsArray (recipe: RecipeModel?) -> [String] {
    var instructionsArray: [String] = []
    if let instructions = recipe?.instructions {
        for step in instructions {
            instructionsArray.append(step.display_text)
        }
    }
    
    return instructionsArray
}



