//
//  RecipeCell.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 18.08.2021.
//

import UIKit

class RecipeCell: UITableViewCell {

    
    @IBOutlet weak var recipeView: UIView!
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func configureRecipeCell(recipe: Recipe) {
        recipeLabel.text = recipe.name
        if let urlString = recipe.thumbnailUrl {
            NetworkRequest.shared.requestData(urlString: urlString) { [weak self] result in
                switch result {
                case .success(let data):
                    let image = UIImage(data: data)
                    self?.recipeImage.image = image
                case .failure(let error):
                    self?.recipeImage.image = nil
                    print("No album image.", error.localizedDescription)
                }
            }
        } else {
            recipeImage.image = recipe.image
        }
    }

}
