//
//  InstructionsTableViewCell.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 29.09.2021.
//

import UIKit

class InstructionsTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var instructionsTextLabel: UILabel!
    @IBOutlet weak var stepNumberLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
