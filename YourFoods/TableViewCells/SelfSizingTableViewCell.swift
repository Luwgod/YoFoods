//
//  SelfSizingTableViewCell.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 29.09.2021.
//

import UIKit

class SelfSizingTableViewCell: UITableViewCell {

    
    
    
    @IBOutlet weak var cellLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
