//
//  RecipeViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 21.08.2021.
//

import UIKit


class RecipeViewController: UIViewController {
    
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func addButtonAction(_ sender: Any) {
        saveRecipe(recipe: recipe!)

    }
    
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeStepsLabel: UILabel!
    @IBOutlet weak var recipeLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var recipeIngredientsLabel: UILabel!
    

    var fromViewNamed: String?
    var recipe: Recipe?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recipeStepsLabel.sizeToFit()
        recipeLabel.text = recipe!.name
        recipeImage.image = recipe!.image
        recipeStepsLabel.text = combineStepsText(instructions: recipe!.instructions)
        recipeIngredientsLabel.text = combineIngredientsText(ingredients: recipe?.ingredients)
        
        switch fromViewNamed {
        case "Find":
            addButton.alpha = 1
        case "Saved":
            addButton.alpha = 0
        default:
            addButton.alpha = addButton.alpha
        }
    }
    
    
    
    
}
