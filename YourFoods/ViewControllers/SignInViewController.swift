//
//  SignInViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 17.09.2021.
//
//  Экран аутентификации в приложение (Книга рецептов)
//  Использовался storyboard

import UIKit
import Firebase

class SignInViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var singinButton: UIButton!
    
    @IBAction func signButtonAction(_ sender: UIButton) {
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { [weak self] authResult, error in
            guard let strongSelf = self else { return }
            if authResult != nil {
                let vc = self?.storyboard?.instantiateViewController(identifier: "MainTabBarController") as! MainTabBarController
                vc.loadTabViews()
                vc.modalPresentationStyle = .fullScreen
                
                self?.present(vc, animated: true, completion: nil)
                
            } else {
                self?.errorLabel.text = "Invalid email or password"
                
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.text = ""
        // Do any additional setup after loading the view.
    }


}

