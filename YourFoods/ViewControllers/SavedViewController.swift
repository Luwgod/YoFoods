//
//  SavedViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 06.12.2021.
//

import UIKit
import Firebase

class SavedViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    @IBOutlet weak var savedRecipesCollectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var savedCategoryCollectionView: UICollectionView!
    @IBOutlet weak var savedRecipesCollectionView: UICollectionView!
    
    
    
    var ref: DatabaseReference?
    let user = Auth.auth().currentUser
    
    var recipesIds = [String: Int]()
    var recipes = [Recipe]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        savedRecipesCollectionView.delegate = self
        savedRecipesCollectionView.dataSource = self
        savedCategoryCollectionView.delegate = self
        savedCategoryCollectionView.dataSource = self
        
        getRecipesIdsFromFirebase()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateSavedRecipes), name: NSNotification.Name(rawValue: "saved"), object: nil)
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case savedRecipesCollectionView:
            return recipes.count
        case savedCategoryCollectionView:
            return 0
        default:
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case savedRecipesCollectionView:
            let cell = savedRecipesCollectionView.dequeueReusableCell(withReuseIdentifier: "SavedRecipeCell", for: indexPath) as! RecipeCollectionViewCell
            cell.configureRecipeCell(recipe: recipes[indexPath.row])
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
    
    
    
    func getRecipesIdsFromFirebase() {
        let user = Auth.auth().currentUser
        let group = DispatchGroup()
        
        
        ref = Database.database().reference()
        group.enter()
        ref?.child("users/\(user!.uid)/saved").getData(completion: { error, snapshot in
          guard error == nil else {
            print(error!.localizedDescription)
            return;
          }
            self.recipesIds = snapshot.value as? [String: Int] ?? [:]
//            print(self.recipesIds)
            group.leave()
        })
        
        group.notify(queue: .main) {
            print(self.recipesIds)
            self.fetchRecipes()
        }
    }
    
    
    func fetchRecipes() {
        let group = DispatchGroup()
        
//        group.enter()
        recipes = []
        for recipeId in recipesIds.values {
//            print(String(recipeId))
            group.enter()
            NetworkDataFetch.shared.fetchResultById(searchedRecipeId: String(recipeId)) { [weak self] resultModel, error in
                
                if error != nil {
                    print(error?.localizedDescription)
                } else {
                    guard let resultModel = resultModel else { return }
                    let recipe = resultModel
                    self?.recipes.append(Recipe(id: recipe.id, name: recipe.name, ingredients: getIngredientsArray(recipe: recipe), instructions: getInstructionsArray(recipe: recipe), image: nil, thumbnailUrl: recipe.thumbnail_url))
                    
//                    print(self?.recipes)
                    group.leave()
                }
            }
        }
        
        group.notify(queue: .main) {
            self.savedRecipesCollectionView.reloadData()
        }
    }
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        savedRecipesCollectionViewHeightConstraint.constant = savedRecipesCollectionView.contentSize.height + 70
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewWillLayoutSubviews()
    }
    
    
    @objc func updateSavedRecipes() {
        getRecipesIdsFromFirebase()
    }

}
