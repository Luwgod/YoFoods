//
//  SearchViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 24.11.2021.
//

import UIKit

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var recipesCollectionView: UICollectionView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var recipeCollectionViewHeightConstraint: NSLayoutConstraint!
    
    
    
    var recipes = [Recipe]()
    var categories = ["Chicken", "Soup", "Beef", "Pork", "Fish", "Sweet", "Summer", "Alcohol", "Ice", "Sandwich", "Breakfast", "Lunch", "Dinner", "Garlic"]

    override func viewDidLoad() {
        super.viewDidLoad()
        recipesCollectionView.delegate = self
        recipesCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        searchTextField.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(fetchFromCategory), name: NSNotification.Name(rawValue: "fetch"), object: nil)

        
        fetchRecipes(recipeName: "chicken")
    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case recipesCollectionView:
            return recipes.count
        case categoryCollectionView:
            return categories.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        
        case recipesCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recipeCell", for: indexPath) as! RecipeCollectionViewCell
            cell.configureRecipeCell(recipe: recipes[indexPath.row])
            return cell
            
        case categoryCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
            cell.configureCategoryCell(categoryName: categories[indexPath.row])
//            cell.layer.cornerRadius = 30
            return cell
            
        default:
            return UICollectionViewCell()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "RecipeViewController") as! RecipeViewController
        vc.recipe = recipes[indexPath.row]
        _ = vc.view
        guard let viewedRecipe = vc.recipe else {
            return
        }
        vc.configureRecipeViewController(recipe: viewedRecipe) {
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.show(vc, sender: Any?.self)
            
//            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    
    func fetchRecipes(recipeName: String) {
        NetworkDataFetch.shared.fetchResult(searchedRecipeName: recipeName) { [weak self] resultModel, error in
            
            if error != nil {
                print(error?.localizedDescription)
            } else {
                guard let resultModel = resultModel else { return }
                self?.recipes = []
                for recipe in resultModel.results {
                    self?.recipes.append(Recipe(id: recipe.id, name: recipe.name, ingredients: getIngredientsArray(recipe: recipe), instructions: getInstructionsArray(recipe: recipe), image: nil, thumbnailUrl: recipe.thumbnail_url))
                }

                self?.recipesCollectionView.reloadData()
            }
        }
    }
    
    
    
    @objc func fetchFromCategory(_ notification: NSNotification) {
        if let category = notification.userInfo?["category"] as? String {
            fetchRecipes(recipeName: category)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        recipeCollectionViewHeightConstraint.constant = recipesCollectionView.contentSize.height + 70
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    

}


extension SearchViewController: UITextFieldDelegate {
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        fetchRecipes(recipeName: text)
    }
    
}


