//
//  ViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 07.09.2021.
//

import UIKit
import Firebase

class TableView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBAction func signOutButtonAction(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            let vc = self.storyboard?.instantiateViewController(identifier: "SignInControllerID") as! SignInViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        } catch {
            print(error)
        }
    }
    
    @IBOutlet weak var signOutButton: UIButton!
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var tableView: UITableView!

    var selectedRecipe = ""
    var searchFailed = false
    private var loading = true
    private var recipesArray = [RecipeModel]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.searchBar.delegate = self
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 25, right: 0)
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loading {
            return 0
        } else if searchFailed {
            return 1
        } else  {
            return recipesArray.count
        }
    }
    

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchFailed {
            let cell = UITableViewCell()
            cell.textLabel?.text = "Nothing found..."
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath) as! RecipeCell

        if loading {
            cell.recipeLabel?.text = "Loading..."
            cell.recipeImage.image = UIImage(named: "LoadingImage")
        } else {
            cell.recipeLabel?.text = recipesArray[indexPath.row].name
//            cell.recipeImage.image = loadImageFromUrlString(urlString: recipesArray[indexPath.row].thumbnail_url)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225.0
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeViewControllerV3") as! RecipeViewController
        let recipe = recipesArray[indexPath.row]
        vc.fromViewNamed = "Find"
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
        }

    
    func fetchRecipes(recipeName: String) {
        NetworkDataFetch.shared.fetchResult(searchedRecipeName: recipeName) { [weak self] resultModel, error in
            
            if error != nil {
                print(error?.localizedDescription)
            } else {
                guard let resultModel = resultModel else { return }
                
                self?.recipesArray = resultModel.results
                self?.tableView.reloadData()
            }
        }
    }
    
}

extension TableView: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchedText = searchBar.text {
            let searchTextForUrl = searchedText.replacingOccurrences(of: " ", with: "%20")
            fetchRecipes(recipeName: searchTextForUrl)
            print("searching")
        }
    }
    
}
