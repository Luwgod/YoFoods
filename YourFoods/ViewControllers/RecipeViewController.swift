//
//  RecipeViewControllerV3.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 18.09.2021.
//

import UIKit

class RecipeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var recipeImageView: UIImageView!
    @IBOutlet weak var ingrTableHeightConstr: NSLayoutConstraint!
    
   
    @IBOutlet weak var instructionsTableHeightConstr: NSLayoutConstraint!
    
    @IBOutlet weak var instructionsTable: UITableView!
    @IBOutlet weak var ingrTable: UITableView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var mainInfoLabel: UILabel!
    
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if let id = recipe?.id {
            saveRecipeToFirebase(recipeId: id)
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "saved"), object: nil)
        
        let alert = UIAlertController(title: "Recipe saved", message: "It can be viewed in saved tab", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    var fromViewNamed: String?
    var recipe: Recipe?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableViews()
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ingrTable {
            return recipe?.ingredients.count ?? 1
        } else if tableView == instructionsTable {
//            print(recipe?.instructions.count)
            return recipe?.instructions.count ?? 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ingrTable {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ingrCell") as! SelfSizingTableViewCell
            cell.cellLabel.text = recipe?.ingredients[indexPath.row]
            return cell
        } else if tableView == instructionsTable {
            let cell = tableView.dequeueReusableCell(withIdentifier: "instructionsCell") as! InstructionsTableViewCell
            cell.instructionsTextLabel.text = recipe?.instructions[indexPath.row]
            let stepNumber = indexPath.row + 1
            cell.stepNumberLabel.text = "\(stepNumber)/\(recipe?.instructions.count ?? 0)"
            if stepNumber == recipe?.instructions.count {
                cell.stepNumberLabel.alpha = 1
            } else {
                cell.stepNumberLabel.alpha = 0.5
            }
            return cell
        }
        return UITableViewCell()
    }
    
    
    func configureRecipeViewController(recipe: Recipe, completion: @escaping () -> ()) {
        self.mainInfoLabel.text = recipe.name
        let group = DispatchGroup()
        
        group.enter()
        guard let urlString = recipe.thumbnailUrl else { return }
        NetworkRequest.shared.requestData(urlString: urlString) { [weak self] result in
            switch result {
            case .success(let data):
                let image = UIImage(data: data)
                self?.recipe?.image = image
                self?.recipeImageView.image = image
                group.leave()
            case .failure(let error):
                self?.recipe?.image = nil
                print(error.localizedDescription)
            }
        }
                
        group.notify(queue: .main) {
            completion()
        }
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.ingrTableHeightConstr?.constant = self.ingrTable.contentSize.height
        self.instructionsTableHeightConstr?.constant = self.instructionsTable.contentSize.height
        
    }
    
    func setTableViews() {
        ingrTable.delegate = self
        ingrTable.dataSource = self
        instructionsTable.delegate = self
        instructionsTable.dataSource = self
        instructionsTable.estimatedRowHeight = 72
    }
    

}
