//
//  MainTabBarController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 30.11.2021.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loadTabViews()
    }
    
    func loadTabViews() {
        for viewController in self.viewControllers ?? [] {
            if let navigationVC = viewController as? UINavigationController, let rootVC = navigationVC.viewControllers.first {
                let _ = rootVC.view
            } else {
                let _ = viewController.view
            }
        }
    }

}
