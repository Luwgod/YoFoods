//
//  LaunchViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 22.10.2021.
//

import UIKit
import Firebase

class LaunchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        if Auth.auth().currentUser != nil {
            let vc = storyboard?.instantiateViewController(identifier: "MainTabBarController") as! MainTabBarController
            vc.loadTabViews()
            vc.modalPresentationStyle = .fullScreen
            
            present(vc, animated: true, completion: nil)
            
        } else {
            let vc = self.storyboard?.instantiateViewController(identifier: "SignInControllerID") as! SignInViewController
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true)
        }
    }

}
