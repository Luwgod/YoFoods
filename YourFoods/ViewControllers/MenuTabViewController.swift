//
//  MenuTabViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 23.11.2021.
//

import UIKit

class MenuTabViewController: UIViewController {

    @IBOutlet var menuView: UIView!
    
    @IBAction func homeButtonAction(_ sender: UIButton) {
        if currentViewController == .home { return }
        currentViewController = .home
        print("home")
        
    }
    @IBAction func searchButtonAction(_ sender: UIButton) {
        if currentViewController == .search { return }
        currentViewController = .search
        print("search")
        let vc = storyboard?.instantiateViewController(identifier: "SearchViewController") as! SearchViewController
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
        
    }
    @IBAction func savedButtonAction(_ sender: UIButton) {
        if currentViewController == .saved { return }
        currentViewController = .saved
        print("saved")
        
    }
    @IBAction func profileButtonAction(_ sender: UIButton) {
        if currentViewController == .profile { return }
        currentViewController = .profile
        print("profile")
        let vc = storyboard?.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
        _ = vc.view
        vc.modalPresentationStyle = .fullScreen
        vc.setView {
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    enum MenuViewControllers {
        case home
        case search
        case saved
        case profile
    }
    var currentViewController = MenuViewControllers.search

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuView.layer.cornerRadius = 30
        
    }
    
    
    

}
