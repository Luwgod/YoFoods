//
//  SavedRecipesViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 08.09.2021.
//

import Foundation
import UIKit
import Firebase

class SavedRecipesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var user: User?
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        if let curUser = Auth.auth().currentUser {
            user = curUser
        }

        print(user?.email)
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "load"), object: nil)
        
        getAllSavedItems()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225.0
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
//            deleteItem(item: models[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath) as! RecipeCell
        let model = models[indexPath.row]
        cell.recipeImage.image = UIImage(data: model.image!)
        cell.recipeLabel.text = model.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        

//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeViewControllerV2") as! RecipeVCv2
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeViewControllerV3") as! RecipeViewController
        
        let model = models[indexPath.row]
        let image = UIImage(data: model.image!)
        vc.modalPresentationStyle = .fullScreen
        vc.fromViewNamed = "Saved"
        present(vc, animated: true)
    }
    

    
    func getAllSavedItems() {
        do {
            models = try context.fetch(RecipeItem.fetchRequest())
        } catch {
        }
    }
    
    
    @objc func loadList(notification: NSNotification){
        getAllSavedItems()
        self.tableView.reloadData()
    }
    

    
    
    
}
