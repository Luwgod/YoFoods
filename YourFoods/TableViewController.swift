//
//  TableViewController.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 18.08.2021.
//

import UIKit

class TableViewController: UITableViewController {
    
    var selectedRecipe = ""
    private var loading = true
    private var recipesArray = [Recipe]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 25, right: 0)
        getDataFromApi()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loading {
            return 1
        } else {
            return recipesArray.count
        }
    }
    

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath) as! RecipeCell

        if loading {
            cell.recipeLabel?.text = "Loading..."
            cell.recipeImage.image = UIImage(named: "LoadingImage")
        } else {
            cell.recipeLabel?.text = recipesArray[indexPath.row].name
            cell.recipeImage.image = loadImageFromUrlString(urlString: recipesArray[indexPath.row].thumbnail_url)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 225.0
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RecipeViewController") as! RecipeViewController
        
        vc.image = (tableView.cellForRow(at: indexPath) as! RecipeCell).recipeImage.image
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
        }

    
    func getDataFromApi() -> Void {
        let headers = [
            "x-rapidapi-host": "tasty.p.rapidapi.com",
            "x-rapidapi-key": "1c0c203cb0msh6104b035bf77881p106367jsn76acab46647a"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://tasty.p.rapidapi.com/recipes/list?from=0&size=10&tags=under_30_minutes&q=ice%20cream")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] (data, response, error) -> Void in
            if let data = data {
                guard let foundRecipes = try? JSONDecoder().decode(ApiResult.self, from: data) else {
                    fatalError("Error decoding data \(error!) ")
                }
                self?.recipesArray = foundRecipes.results
            }
            self?.loading = false
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        })
        
        dataTask.resume()
        
    }

}
    


