//
//  RecipeItem+CoreDataProperties.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 09.09.2021.
//
//

import Foundation
import CoreData


extension RecipeItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecipeItem> {
        return NSFetchRequest<RecipeItem>(entityName: "RecipeItem")
    }

    @NSManaged public var name: String?
    @NSManaged public var instructions: [String]?
    @NSManaged public var image: Data?
    @NSManaged public var ingredients: [String]?

}

extension RecipeItem : Identifiable {

}
