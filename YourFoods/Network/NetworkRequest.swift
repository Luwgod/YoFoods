//
//  NetworkRequest.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 23.11.2021.
//

import Foundation

class NetworkRequest {
    
    static let shared = NetworkRequest()
    
    private init() {}
    
    func requestData(urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, responce, error in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else { return }
                completion(.success(data))
            }
        }
        
        .resume()
        
    }
    
    func apiRequest(searchedRecipeName: String?, completion: @escaping (Result<Data, Error>) -> Void) {
        
        // API info
        // change if needed
        
        let headers = [
            "x-rapidapi-host": "tasty.p.rapidapi.com",
            "x-rapidapi-key": "1c0c203cb0msh6104b035bf77881p106367jsn76acab46647a"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://tasty.p.rapidapi.com/recipes/list?from=0&size=5&tags=under_30_minutes&q=\(searchedRecipeName!)")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in
            
            DispatchQueue.main.async {
                if error != nil {
                    completion(.failure(error as! Error))
                }
                
                guard let data = data else { return }
                completion(.success(data))
            }
            
        }
        
        dataTask.resume()
        
    }
    
    
    func apiRequestById (searchedRecipeId: String?, completion: @escaping (Result<Data, Error>) -> Void) {
        
        // API info
        // change if needed
        
        let headers = [
            "x-rapidapi-host": "tasty.p.rapidapi.com",
            "x-rapidapi-key": "1c0c203cb0msh6104b035bf77881p106367jsn76acab46647a"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://tasty.p.rapidapi.com/recipes/detail?id=\(searchedRecipeId ?? String(5586))")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in
            
            DispatchQueue.main.async {
                if error != nil {
                    completion(.failure(error as! Error))
                }
                
                guard let data = data else { return }
                completion(.success(data))
            }
            
        }
        
        dataTask.resume()
        
    }
    
}


// "https://tasty.p.rapidapi.com/recipes/detail?id=5586"
