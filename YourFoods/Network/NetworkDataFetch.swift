//
//  NetworkDataFetch.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 23.11.2021.
//

import Foundation

class NetworkDataFetch {
    
    static let shared = NetworkDataFetch()
    
    private init() {}
    
    func fetchResult (searchedRecipeName: String, responce: @escaping (ApiResult?, Error?) -> Void) {
        
        NetworkRequest.shared.apiRequest(searchedRecipeName: searchedRecipeName) { result in
            
            switch result {
            
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(ApiResult.self, from: data)
                    responce(result, nil)
                } catch let jsonError {
                    print("JSON decode failure.", jsonError)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
    
    func fetchResultById (searchedRecipeId: String, responce: @escaping (RecipeModel?, Error?) -> Void) {
        
        NetworkRequest.shared.apiRequestById(searchedRecipeId: searchedRecipeId) { result in
            
            switch result {
            
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(RecipeModel.self, from: data)
                    responce(result, nil)
                } catch let jsonError {
                    print("JSON decode failure.", jsonError)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
    
}
