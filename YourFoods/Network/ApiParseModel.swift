//
//  ApiParseModel.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 02.09.2021.
//

import Foundation


struct ApiResult: Codable {
    let count: Int
    let results: [RecipeModel]
}

struct RecipeModel: Codable {
    let id: Int
    let name: String
    let thumbnail_url: String?
    let instructions: [Step]?
    let sections: [SectionItem]?
}

struct Step: Codable {
    let position: Int
    let display_text: String
}

struct SectionItem: Codable {
    let components: [ComponentItem]
}

struct ComponentItem: Codable {
    let raw_text: String
}



