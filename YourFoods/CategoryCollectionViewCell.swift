//
//  CategoryCollectionViewCell.swift
//  YourFoods
//
//  Created by Sasha Styazhkin on 26.11.2021.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBAction func categoryNameButtonAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fetch"), object: nil, userInfo: ["category": categoryNameButton.currentTitle])
    }
    
    
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var categoryNameButton: UIButton!
    
    
    func configureCategoryCell(categoryName: String) {
        self.categoryNameButton.setTitle(categoryName, for: .normal)
        
        self.cellContentView.layer.cornerRadius = 7
    }
    
}
