Project currently in progress...

// Note: Currently app might not work because it ran out of free monthly api requests

// Note 2: project is relatively old. Consider visiting PizzaTestTask project for my current coding skills

CURRENT FEATURES :
- Search for wanted recipe in Search tab 🔎
 - You can also try one of suggested searches 🧾

- Edit your profile in Profile tab (only image is editable currently) 👨
- View recipes you have saved in Saved tab 🍱

- All user's data is stored safe in Google Firebase 🔐


UPCOMING FEATURES :
- More profile editable information
- Editable Saved recipes tab
- Home tab with news and suggestions (currently blank)
- Offline mode using CoreData


Technology Stack:
1. UIKit, Storyboard
2. Firebase
3. Tasty Api with recipes
4. Network tasks with URLSession
5. MVC architecture
# Designed in Figma by Alex Kochnev.

Screen Cast of the app can be viewed in Google Drive:
    https://drive.google.com/file/d/1raKapHMQXe1n_Z1hN8N0PdA6GKD_Vpnt/view?usp=sharing
